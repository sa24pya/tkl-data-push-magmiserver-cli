# TKL data-push-magmiservwer-cli
TKL data-push-magmiserver-cli  
TKL data-push-magmiserver-cli provides serverside magmi entry point.  
Support PSR-1, PSR-2, PSR-3, PSR-4.

![sa24 package repository](https://www.dream-destination-wedding.com/images/exotic2.jpg)

## Install

Via Composer

``` bash
$ composer require tkl/data-push-magmiserver-cli
```

## Usage

``` bash
$ php
$ 
```

## Testing

``` bash
$ phpunit
```

## Contributing


## Credits

- [Wilson Smith](https://github.com/wilsonsmith)
- [All Contributors](ttps://mma-sa24@bitbucket.org/tkl/data-push-magmiserver-cli.git/contributors)

## License

The GNU General Public Licence version 3 (GPLv3). Please see [License File](LICENSE.md) for more information.
