<?php
require_once(__DIR__."/../../inc/magmi_defs.php");
//Datapump include
require_once(__DIR__."/../inc/magmi_datapump.php");
if ($_POST) {
    try {
        $dp=Magmi_DataPumpFactory::getDataPumpInstance("productimport");
        $productItems = json_decode($_POST['data'], true);
        $dp->beginImportSession("Default", "Local"); // could add a Logger as a 3rd parameter
        $pid = [];
        $result = [];
        print_r("Starting import...\n");
        foreach ($productItems as $productItem) {
            // Now ingest item into magento
            // print_r("Trying to ingest...\n");
            $productItem =  array_change_key_case($productItem, CASE_LOWER);
            $productItem["sku"] = $productItem["gtin"];
            $productItem["attribute_set"] = "GS1";
            $productItem["_attribute_set"] = "GS1";
            // print_r($productItem);
            $result = $dp->ingest($productItem);
            // print_r("Ingest...\n");
            if ($result['ok'] === true) {
                $pid[] = $productItem["gtin"];
            } else {
                var_dump($result);
                throw new \Exception("Magmi error: Magmi_DataPump ingested " . $result["last"] . " items.");
            }
        }
        print_r(count($pid)." imported of ".count($productItems)." items.\n");
        // End import Session
        $dp->endImportSession();
    } catch (\Exception $e) {
        print_r("An error happened!\n");
        print_r($e->getMessage()."\n");
        print_r(count($pid)." imported of ".count($productItems)." items.\n");
        http_response_code(403);
    }
} else {
    echo "This script is meant to be used via POST";
}
