Set up your local directory

Set up Git on your machine if you haven't already.

mkdir /path/to/your/projectcd /path/to/your/project
git initgit remote add origin https://mma-sa24@bitbucket.org/sa24pya/tkl-data-push-magmiserver-cli.git

Create your first file, commit, and push

echo "Matteo M." >> contributors.txtgit add contributors.txtgit commit -m 'Initial commit with contributors'git push -u origin master

Great job, now you're all set up! Get started coding or create a team and invite people to work with you.

